# include "MyQueue.h"
# include <stdlib.h>
# include <iostream>
# include "Customer.h"
using namespace std;
MyQueue::MyQueue(int queueCapacity)
{
	//构造函数重要的是分配内存；
	m_iQueueCapacity = queueCapacity;
//	m_iHead = 0;
//	m_iTail = 0;
//	m_iQueueLen = 0;
//与上面一块等价。
	m_iHead = 0;
	m_iTail = 0;
	m_iQueueLen = 0;
	m_pQueue = new Customer[m_iQueueCapacity];//调了一天的问题，定位到堆内存的问题，愣是没找到，后来一看原来申请堆时忘记给内存，傻！前往别忘记队列是需要初始化内存的

}
MyQueue::~MyQueue()//virtual不用在类外再定义。
{
	delete[] m_pQueue;
	m_pQueue = NULL;
}
void MyQueue::ClearQueue()
{
	m_iHead = 0;
	m_iTail = 0;
	m_iQueueLen = 0;
}
bool MyQueue::QueueEmpty() const
{
	if (m_iQueueLen == 0)
		return true;
	return false;
}
bool  MyQueue::QueueFull() const
{
	if (m_iQueueLen == m_iQueueCapacity)
		return true;
	return false;
}
int MyQueue::QueueLength() 
{
	return m_iQueueLen;
}
bool MyQueue::EnQueue(Customer element)
{
	//添加新元素，首先头和尾都指向[0]这里。然后添加元素，队尾+1，队头不变。
	//首先要判断是否为满。
	//添加元素到队尾所在的位置。
	//队尾+1。
	if (QueueFull())
		return false;
	else
	{
		m_pQueue[m_iTail] = element;
		m_iTail++;
		m_iTail = m_iTail % m_iQueueCapacity;//因为是一个环形队列，到容量最大值时+1要回到[0].
		m_iQueueLen++;
		return true;
	}
	
}
bool MyQueue::DeQueue(Customer &element)
{
	if (QueueEmpty())
		return false;
	element = m_pQueue[m_iHead];
	m_iHead++;
	m_iHead = m_iHead % m_iQueueCapacity;
	m_iQueueLen--;
	return true;
}
//这段程序很迷
void MyQueue::QueueTraverse()
{
	cout << endl;
	
	for (int i = m_iHead; i < m_iQueueLen + m_iHead; i++)
	{
		m_pQueue[i % m_iQueueCapacity].printInfo() ;
		cout << "前面还有" << (i - m_iHead) << "人" << endl;
	}
	cout << endl;
}
		