# include <iostream>
# include <stdlib.h>
# include "MyQueue.h"
# include "Customer.h"
using namespace std;
int main(void)
{
	MyQueue *p = new MyQueue(4);
	Customer customer1("zhangsan",20);
	Customer customer2("lisi", 30);
	Customer customer3("wangwu", 40);
	p->EnQueue(customer1);
	p->EnQueue(customer2);
	p->EnQueue(customer3);
	p->QueueTraverse();

	Customer customer4("", 0);
	p->DeQueue(customer4);
	customer4.printInfo();
	p->QueueTraverse();

	//int temp=0;
	//p->DeQueue(temp);	
	//cout << "temp=" << temp << endl;

	
	//p->DeQueue(temp);
	//cout << "temp=" << temp<< endl;
	//p->QueueTraverse();
	
	delete p;
	p = NULL;	
	//system("pause");
	return 0;

}