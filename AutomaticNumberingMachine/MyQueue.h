# ifndef MYQUEUE_H
# define MYQUEUE_H
# include "Customer.h"
class MyQueue
{
public:
	MyQueue(int queueCapacit=0);
	virtual ~MyQueue();//定义了多态！貌似没意义吧
	void ClearQueue();
	bool QueueEmpty() const;//为什么要加入const？这个函数不会修改类中的数据成员
	bool QueueFull() const;
	int QueueLength();
	bool EnQueue(Customer element);
	bool DeQueue(Customer &element);
	void QueueTraverse();
private:
	Customer *m_pQueue;//指向队列，给队列分配资源
	int m_iQueueLen;
	int m_iQueueCapacity;
	int m_iHead;
	int m_iTail;

};



# endif;